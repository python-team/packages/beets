Source: beets
Section: sound
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Stefano Rivera <stefanor@debian.org>,
 Ryan Kavanagh <rak@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 pybuild-plugin-pyproject,
 dh-sequence-python3,
 python3-all,
 python3-bs4 <!nocheck>,
 python3-confuse (>= 1.5) <!nocheck>,
 python3-discogs-client <!nocheck>,
 python3-flask <!nocheck>,
 python3-jellyfish <!nocheck>,
 python3-mediafile <!nocheck>,
 python3-mpd <!nocheck>,
 python3-munkres <!nocheck>,
 python3-musicbrainzngs <!nocheck>,
 python3-platformdirs <!nocheck>,
 python3-poetry-core,
 python3-py7zr <!nocheck>,
 python3-pydata-sphinx-theme <!nodoc>,
 python3-pylast <!nocheck>,
 python3-pytest <!nocheck>,
 python3-pytest-flask <!nocheck>,
 python3-rarfile <!nocheck>,
 python3-reflink <!nocheck>,
 python3-requests-oauthlib <!nocheck>,
 python3-responses <!nocheck>,
 python3-sphinx <!nodoc>,
 python3-unidecode (>= 1.3.6) <!nocheck>,
 python3-xdg <!nocheck>,
 python3-yaml <!nocheck>
Standards-Version: 4.6.2
Homepage: https://beets.io/
Vcs-Git: https://salsa.debian.org/python-team/packages/beets.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/beets
Rules-Requires-Root: no

Package: beets
Architecture: all
Depends:
 libjs-backbone,
 libjs-jquery,
 libjs-underscore,
 python3-confuse (>= 1.5),
 python3-mediafile,
 ${misc:Depends},
 ${python3:Depends}
Suggests:
 beets-doc,
 ffmpeg,
 mp3gain,
 python3-acoustid,
 python3-bs4,
 python3-dbus,
 python3-discogs-client,
 python3-flask,
 python3-flask-cors,
 python3-gi,
 python3-gst-1.0,
 python3-langdetect,
 python3-mpd,
 python3-mutagen,
 python3-pil,
 python3-py7zr,
 python3-pylast,
 python3-rarfile,
 python3-reflink,
 python3-requests,
 python3-requests-oauthlib,
 python3-resampy,
 python3-xdg
Description: music tagger and library organizer
 Beets is a media library management system for obsessive-compulsive music
 geeks.
 .
 The purpose of beets is to get your music collection right once and for all.
 It catalogs your collection, automatically improving its metadata as it goes
 using the MusicBrainz database.  It then provides a set of tools for
 manipulating and accessing your music.
 .
 Beets also includes a music player that implements the MPD protocol, so you
 can play music in your beets library using any MPD client.

Package: beets-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends}
Description: music tagger and library organizer - documentation
 Beets is a media library management system for obsessive-compulsive music
 geeks.
 .
 The purpose of beets is to get your music collection right once and for all.
 It catalogs your collection, automatically improving its metadata as it goes
 using the MusicBrainz database.  It then provides a set of tools for
 manipulating and accessing your music.
 .
 Beets also includes a music player that implements the MPD protocol, so you
 can play music in your beets library using any MPD client.
 .
 This package provides detailed documentation on beets usage.
